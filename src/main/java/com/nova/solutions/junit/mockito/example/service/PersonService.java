package com.nova.solutions.junit.mockito.example.service;

import com.nova.solutions.junit.mockito.example.model.Person;
import com.nova.solutions.junit.mockito.example.util.Util;

public class PersonService {
  
  public Person person(int id, String name, String lastName, String birthDate, int statusCode) {
    Person person = null;
    try {
      person = new Person(id, name, lastName, birthDate, Util.getAge(birthDate), Util.getStatus(statusCode), statusCode); 
    } catch (Exception ex) {
      System.out.println("Error en: " + ex.getMessage());
    }
    return person;
  }

}

package com.nova.solutions.junit.mockito.example.model;


public class Person {
  
  public int id;
  
  public String name;
  
  public String lastName;
  
  public String birthDate;
  
  public int age;
  
  public int statusCode;
  
  public String status;
  
  public Person() {
    
  }

  public Person(int id, String name, String lastName, String birthDate, int age, String status, int statusCode) {
    super();
    this.id = id;
    this.name = name;
    this.lastName = lastName;
    this.birthDate = birthDate;
    this.age = age;
    this.status = status;
    this.statusCode = statusCode;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(String birthDate) {
    this.birthDate = birthDate;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }
}
